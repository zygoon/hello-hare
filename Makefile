# SPDX-License-Identifier: BSD-3-Clause 
# SPDX-FileCopyrightText: Zygmunt Krynicki <me@zygoon.pl>

prefix ?= /usr/local
bindir ?= $(prefix)/bin
DESTDIR ?=

all:: hello-hare
clean::
	rm -f hello-hare
install: $(DESTDIR)$(bindir)/hello-hare

hello-hare: hello-hare.ha
	hare build -o $@ $<

$(DESTDIR)$(bindir)/hello-hare: hello-hare | $(DESTDIR)$(bindir)
	install -m 755 $< $@
$(DESTDIR)$(bindir):
	mkdir -p $@
